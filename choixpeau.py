import random
from tkinter import Frame, Tk, BOTH, Label
import sys
from PIL import Image, ImageTk
from playsound import playsound

nb_users = int(sys.argv[1]) if len(sys.argv) > 1 else 11
nb_gryff = nb_users // 2
assignments = ["gryffondor"] * nb_gryff + ["poufsouffle"] * (nb_users - nb_gryff)
random.shuffle(assignments)
print(assignments)


class Window(Frame):

    def __init__(self, master=None, **kw):
        super().__init__(master, **kw)
        self.pack(fill=BOTH, expand=True)
        self.configure(background='black')

        master.update_idletasks()
        self._show_image("intro", sound=False)
        self.position_in_assignments = -1
        self.started = False

    def _show_image(self, name, sound=True):
        for child in self.winfo_children():
            child.destroy()
        image = Image.open(f"{name}.png")
        window_height = self.winfo_height()
        new_width = window_height * image.width // image.height
        image = image.resize((new_width, window_height), Image.ANTIALIAS)
        image_tk = ImageTk.PhotoImage(image)
        label = Label(self, image=image_tk)
        label.configure(background='black')
        label.image = image_tk
        label.place(x=self.winfo_width() / 2 - image.width / 2, y=window_height / 2 - image.height / 2)
        if sound:
            self.after(100, lambda: playsound(f"{name}.wav"))

    def change_image(self):
        if not self.started:
            self._show_image("intro")
            self.started = True
            return
        self.position_in_assignments += 1
        if self.position_in_assignments < len(assignments):
            self._show_image(assignments[self.position_in_assignments])
            self.after(1000 * 5, lambda: self._show_image("intro", sound=False))
        else:
            self._show_image("end")


root = Tk()
root.attributes('-fullscreen', True)
root.bind("<F11>", lambda _: root.attributes("-fullscreen", True))
root.bind("<Escape>", lambda _: root.attributes("-fullscreen", False))
root.bind("q", lambda _: root.quit())
app = Window(root)
root.bind("<space>", lambda _: app.change_image())
root.wm_title("Choixpeau")
root.mainloop()
